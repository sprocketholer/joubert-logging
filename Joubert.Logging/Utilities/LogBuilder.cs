﻿// <copyright file="LogBuilder.cs" company="Stephen J. Joubert">
//     Copyright (c) Stephen J. Joubert. All rights reserved.
// </copyright>

namespace Joubert.Logging.Utilities
{
    using System;
    using System.Text;

    /// <summary>
    /// Log builder utility
    /// </summary>
    public class LogBuilder
    {
        /// <summary>
        /// Transforms an exception into a log message
        /// </summary>
        /// <param name="exception">An instance of the <see cref="Exception"/> class</param>
        /// <returns>A the log message for the exception</returns>
        public static string GetLog(Exception exception)
        {
            var logMessage = new StringBuilder();
            var format = "{2}{0}: {1}";

            if (exception != null)
            {
                logMessage.AppendLine("[EXCEPTION]");
                logMessage.AppendFormat(format, "MESSAGE", exception.Message, string.Empty);
                logMessage.AppendFormat(format, "SOURCE", exception.Source, ", ");
                logMessage.AppendFormat(format, "STACK TRACE", exception.StackTrace, ", ");

                var baseException = exception.GetBaseException();
                if (baseException != exception)
                {
                    logMessage.Append("***INNER EXCEPTION***");
                    logMessage.AppendFormat(format, "; INNER EXCEPTION MESSAGE", baseException.Message, ", ");
                    logMessage.AppendFormat(format, "INNER EXCEPTION SOURCE", baseException.Source, ", ");
                    logMessage.AppendFormat(format, "INNER EXCEPTION STACK TRACE", baseException.StackTrace, ", ");
                }
            }

            return logMessage.ToString();
        }
    }
}
